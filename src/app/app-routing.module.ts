import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GraphComponent } from './component/graph/graph.component';
import { LiveGraphComponent } from './component/live-graph/live-graph.component';
import { StartComponent } from './component/start/start.component';
import { TestGraphComponent } from './component/test-graph/test-graph.component';
import { UserResolver } from './resolver/user';
import { GraphRealDataComponent } from './component/graph-real-data/graph-real-data.component';

const routes: Routes = [
  { path: '', component: StartComponent },
  { path: 'livegraph', component: LiveGraphComponent },
  { 
    path: 'livegraph/:id', 
    component: LiveGraphComponent,
    resolve: { resolverUser: UserResolver } 
   },
  { path: 'start', component: StartComponent },
  { path: 'testgraph', component: TestGraphComponent},
  { 
    path: 'realdata/:id', 
    component: GraphRealDataComponent,
    resolve: { resolverUser: UserResolver } 
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
