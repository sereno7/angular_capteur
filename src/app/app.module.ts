import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GraphComponent } from './component/graph/graph.component';
import { ChartsModule } from 'ng2-charts';
import { HttpClientModule } from '@angular/common/http';
import { LiveGraphComponent } from './component/live-graph/live-graph.component';
import { StartComponent } from './component/start/start.component';
import { TestGraphComponent } from './component/test-graph/test-graph.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { NbThemeModule, NbLayoutModule, NbCardModule, NbToastrModule, NbButtonComponent, NbButtonModule, NbInputModule, NbMenuModule, NbSidebarModule, NbCheckboxModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { GraphRealDataComponent } from './component/graph-real-data/graph-real-data.component';


@NgModule({
  declarations: [
    AppComponent,
    GraphComponent,
    LiveGraphComponent,
    StartComponent,
    TestGraphComponent,
    GraphRealDataComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ChartsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NbCardModule,
    ReactiveFormsModule,
    NbThemeModule.forRoot({ name: 'dark' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbToastrModule.forRoot(),
    NbButtonModule,
    NbInputModule,
    NbMenuModule.forRoot(),
    NbSidebarModule.forRoot(),
    NbCheckboxModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
