import { Component } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  items: NbMenuItem[]
  constructor(){}

  ngOnInit(): void {
    this.items = [
      { title: 'LiveGraph', link: '/start', icon: 'sun'},
      { title: 'History', link: '/testgraph', icon: 'book'},          
      { title: 'realdata', link: '/realdata', icon: 'book'},          
    ];
  }
}
