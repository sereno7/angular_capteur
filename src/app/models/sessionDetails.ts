import { Entry } from "./entry";

export interface SessionsDetails{
    id?: number,
    time: Date,
    idUser: number,
    entryList: Entry[];
}