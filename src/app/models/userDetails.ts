import { Sessions } from "../models/sessions";

export interface UserDetails{
    id: number,
    name: string,
    password: number,
    sessionsList: Sessions[],
}