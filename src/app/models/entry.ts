export interface Entry{
    id: number,
    time: string,
    heartRate: number,
    idSession:number
}