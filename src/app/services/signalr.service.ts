import { EventEmitter, Injectable } from '@angular/core';  
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';  
import { Entry } from '../models/entry'
import { environment } from '../../environments/environment';
 
@Injectable({
  providedIn: 'root'
})  
export class SignalRService {  
  messageReceived = new EventEmitter<Entry>();  
  connectionEstablished = new EventEmitter<Boolean>();  
  
  private connectionIsEstablished = false;  
  public _hubConnection: HubConnection;  
  url: string = environment.SIGNALR_URL;
  
  constructor() {  
    this.createConnection();  
    this.registerOnServerEvents();  
    // this.startConnection();  
  }  
  
  sendChatMessage(message: Entry) {  
    this._hubConnection.invoke('SendMessage', message);  
  }  
  
  private createConnection() {  
    this._hubConnection = new HubConnectionBuilder()  
      .withUrl(this.url)
      .build();  
  }  
  
  public startConnection(): void {  
    console.log(this._hubConnection);
    this._hubConnection  
      
      .start()  
      .then(() => {  
        this.connectionIsEstablished = true;  
        console.log('Hub connection started');  
        this.connectionEstablished.emit(true);  
      })  
      .catch(err => {  
        console.log(err);  
        // setTimeout(this.startConnection(), 5000);  
      });  
  }  
  
  private registerOnServerEvents(): void {  
    this._hubConnection.on('ReceiveMessage', (data: any) => {  
      this.messageReceived.emit(data);  
    });  
  }  

  public stopConnection(){
    this._hubConnection.stop();
  }
}  
