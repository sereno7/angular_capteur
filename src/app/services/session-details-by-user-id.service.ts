import { Injectable } from '@angular/core';
import { SessionsDetails } from '../models/sessionDetails';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SessionDetailsByUserIdService {
  context : BehaviorSubject<SessionsDetails[]>;
  private url : string = environment.SESSIONS_DETAIL_BYUSERID_BASE__URL;
  
  constructor(private http: HttpClient) {
    // this.context = new BehaviorSubject<SessionsDetails[]>(null);
    // this.refresh();
   }

   refresh() {
    this.http.get<SessionsDetails[]>(this.url).subscribe(data => {
      this.context.next(data);
    });
  }

  getSessionDetailByUserID(id: number){
    console.log(id);
    
    console.log(42);
    
    return this.http.get<SessionsDetails[]>(this.url + id)
  }

}
