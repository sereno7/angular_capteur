import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Entry } from '../models/entry';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EntryService {
  context : BehaviorSubject<Entry[]>;
  private url : string = environment.ENTRY_BASE__URL;
  
  constructor(private http: HttpClient) {
    this.context = new BehaviorSubject<Entry[]>(null);
    this.refresh();
   }

   refresh() {
    this.http.get<Entry[]>(this.url).subscribe(data => {
      this.context.next(data);
    });
  }
  add(entry:Entry[]){
    return this.http.post(this.url, entry);
  }

}
