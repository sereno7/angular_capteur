import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Entry } from '../models/entry';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  context : BehaviorSubject<User[]>;
  private url : string = environment.USER_BASE__URL;
  
  constructor(private http: HttpClient) {
    this.context = new BehaviorSubject<User[]>(null);
    this.refresh();
   }

   refresh() {
    this.http.get<User[]>(this.url).subscribe(data => {
      this.context.next(data);
    });
  }

  add(user:User){
    return this.http.post<number>(this.url, user);
  }

  getbyid(id:number) {
    return this.http.get<User>(this.url + "/byid/?id=" + id)
  }
}
