import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Entry } from '../models/entry';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { Sessions } from '../models/sessions';


@Injectable({
  providedIn: 'root'
})
export class SessionsService {
  context : BehaviorSubject<Sessions[]>;
  private url : string = environment.SESSIONS_BASE__URL;
  
  constructor(private http: HttpClient) {
    this.context = new BehaviorSubject<Sessions[]>(null);
    this.refresh();
   }

   refresh() {
    this.http.get<Sessions[]>(this.url).subscribe(data => {
      this.context.next(data);
    });
  }

  add(sessions:Sessions){
    return this.http.post<number>(this.url, sessions);
  }

  delete(id:number){
    console.log(42);
    console.log(id);
    return this.http.delete(this.url+ "delete/" + id)
  }
}