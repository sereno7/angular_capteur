import { Resolve, ActivatedRoute } from "@angular/router";
import { Injectable } from "@angular/core";
import { User } from "../models/user";
import { UserService } from "../services/users.service";


@Injectable({
    providedIn: 'root'
  })
  
export class UserResolver implements Resolve<User>{
    constructor(private userService:UserService){}
    resolve(route: import("@angular/router").ActivatedRouteSnapshot, state: import("@angular/router").RouterStateSnapshot)
    : User | import("rxjs").Observable<User> | Promise<User> {
        return this.userService.getbyid(route.params.id);       
    }  
}