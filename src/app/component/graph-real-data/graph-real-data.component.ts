import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets, ChartType, ChartOptions } from 'chart.js';
import { BaseChartDirective, Color, Label } from 'ng2-charts';
import { SessionsService } from '../../services/sessions.service';
import { User } from '../../models/user';
import { Route, ActivatedRoute } from '@angular/router';
import { SessionsDetails } from '../../models/sessionDetails';
import { SessionDetailsByUserIdService } from '../../services/session-details-by-user-id.service';

@Component({
  selector: 'app-graph-real-data',
  templateUrl: './graph-real-data.component.html',
  styleUrls: ['./graph-real-data.component.scss']
})
export class GraphRealDataComponent implements OnInit {
  currentUser:User;

  public lineChartData: ChartDataSets[] = [
    {data: [], label: ""},
    {data: [], label: ""},
    {data: [], label: ""}
  ];

  // public lineChartLabels: Label[] = ['0:00', '0:30', '1:00', '1:30', '2:00', '2:30', '3:00'];
  public lineChartLabels: Label[] = [];

  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    annotation: "",
    responsive: true,
    scales: {
      yAxes: [
        {
          position: 'left',
          
          ticks: {
            max: 100,
            min: 40
          }
        }
      ]
    },
  };
  // public opacity1;
  public lineChartColors: Color[] = [
    { // yellow
      // backgroundColor: 'rgba(240, 232, 10,0.2)',
      backgroundColor: 'rgba(240, 232, 10,0.2)',
      borderColor: 'rgba(240, 232, 10,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // red
      // backgroundColor: 'rgba(255,0,0,0.3)',
      backgroundColor: 'rgba(255,0,0,0.2)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // green
      // backgroundColor: 'rgba(11, 191, 59,0.2)',
      backgroundColor: 'rgba(11, 191, 59,0.2)',
      borderColor: 'rgba(11, 191, 59,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
  ];
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public test: SessionsDetails[];

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  constructor(
    private sessionsDetails: SessionDetailsByUserIdService,
    private route: ActivatedRoute,
    ) {  }

  public selectedDay: string = '';
  
  public buttons = document.getElementsByTagName('button');
  public ranges = document.querySelectorAll("input[type=range]");
  public opacity = [0.2, 0.2, 0.2];

  changeOpacity (event: any) {
    let order = event.target.getAttribute("order");
    let opacity = this.opacity[order - 1];
    opacity = event.target.value;
    this.opacity[order - 1] = opacity;
    this.lineChartColors[0].backgroundColor = `rgba(240, 232, 10,${this.opacity[0]})`;
    this.lineChartColors[1].backgroundColor = `rgba(255, 0, 0,${this.opacity[1]})`;
    this.lineChartColors[2].backgroundColor = `rgba(11, 191, 59,${this.opacity[2]})`;
  }

  public selectedValues = [];
  selectChangeHandler (event: any) {

    const e = event.target;
  
    this.selectedDay = e.value;

    // let pos = this.test.map(function(e) { return e.time; }).indexOf(new Date(this.selectedDay));
    let pos = this.test.map(function(e) { return e.time.toString(); }).indexOf(this.selectedDay);

  
    const order = e.getAttribute("order");
    switch (order) {

      case "1":
        this.lineChartData[0].data = [];
        
          for (const entry of this.test[pos].entryList) {            
            this.lineChartData[0].data.push(entry.heartRate);
            this.lineChartLabels = [];
            this.lineChartLabels.push(entry.time);
          }
        this.lineChartData[0].label = this.test[pos].time.toString();
        this.selectedValues[0] = this.test[pos].time;
        break;
        
      case "2":
        this.lineChartData[1].data = [];
        for (const entry of this.test[pos].entryList) {            
          this.lineChartData[1].data.push(entry.heartRate);
          this.lineChartLabels = [];
          this.lineChartLabels.push(entry.time);
        }
        this.lineChartData[1].label = this.test[pos].time.toString();
        this.selectedValues[1] = this.test[pos].time;
        break;

      case "3":
        this.lineChartData[2].data = [];
        for (const entry of this.test[pos].entryList) {            
          this.lineChartData[2].data.push(entry.heartRate);
          this.lineChartLabels = [];
          this.lineChartLabels.push(entry.time);
        }
        this.lineChartData[2].label = this.test[pos].time.toString();
        this.selectedValues[2] = this.test[pos].time;
        break;
    }

    const isHidden = this.chart.isDatasetHidden(order - 1);
    if(!isHidden) {
      this.buttons[order - 1].disabled = false;
      let ranges = (document.querySelectorAll("input[type=range]"));
      (ranges[order - 1] as HTMLInputElement).disabled = false;
      this.buttons[order - 1].innerText = "Masquer"
    }
  }

  clickedSelect() {
    for(let i = 0; i < this.selects.length; i++) {
      for(let j = 1; j < this.selects[i].options.length; j++) {
          if(this.selects[i].options[j].value !== this.selectedValues[i] && this.selects[i].options[j].value !== "Choose your record") {
            this.selects[i].options[j].disabled = false;
          }
      }
    }
    for(let i = 0; i < this.selects.length; i++) {
      for(let j = 1; j < this.selects[i].options.length; j++) {
        if(this.selectedValues[i] !== null) {
          for(let k = 0; k < this.selectedValues.length; k++) {
            if(this.selects[i].options[j].value == this.selectedValues[k]) {
              this.selects[i].options[j].disabled = true;
            }
          }
        }
      }
    }

  }
  public selects = document.getElementsByTagName("select");


  ngOnInit(): void {
    this.currentUser = this.route.snapshot.data.resolverUser;
    console.log(this.currentUser);
    
     this.sessionsDetails.getSessionDetailByUserID(this.currentUser.id).subscribe(data =>{
      this.test = data;
      console.log(this.test);
      console.log(this.test[3].entryList);
    });
    
  }

  public hideOne(event: any){
    // let pos: number = this.test.map(function(e) { return e.time; }).indexOf(this.selectedDay);
    let pos: number = 1;
    const order = event.target.getAttribute("order");
    const isHidden = this.chart.isDatasetHidden(order - 1);
    this.chart.hideDataset(order - 1, !isHidden);
    if(!isHidden) {
      this.buttons[order - 1].innerText = "Afficher";
    }
    else {
      this.buttons[order - 1].innerText = "Masquer";
    }
  }
}

