import { Component, OnInit, OnDestroy } from '@angular/core';
import { SignalRService } from '../../services/signalr.service'
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import { Entry } from '../../models/entry';
import { ActivatedRoute, Router } from '@angular/router';
import { EntryService } from '../../services/entry.service';
import { NbToastrService } from '@nebular/theme';
import { SessionsService } from '../../services/sessions.service';
import { User } from '../../models/user';
import { LogLevel } from '@aspnet/signalr';


@Component({
  selector: 'app-live-graph',
  templateUrl: './live-graph.component.html',
  styleUrls: ['./live-graph.component.scss']
})
export class LiveGraphComponent implements OnInit, OnDestroy {
  ngOnDestroy(): void {
    this.signalrService.stopConnection();
  }
  tab: Entry[] = [];
  entry:Entry;
  isStarted:boolean;
  timer:number;
  interval;
  idSession:number;
  currentUser:User;
  t:number;

  constructor(
    private signalrService: SignalRService,
    private route: ActivatedRoute,
    private entryService: EntryService,
    private toastr: NbToastrService,
    private sessionService: SessionsService,
    private router: Router,

    ) { }

  public lineChartData: ChartDataSets[] = [
    { data: [], label: 'Live Graph' },
  ];
  public lineChartLabels: Label[] = [];

  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    annotation: "",
    scales: {
      yAxes: [{
        ticks: {
           max: 100,
           min: 40
        }
     }],
     xAxes: [{
      ticks: {
         max: 100,
         min: 40
      }
   }]
  }
}

  ngOnInit(): void {
    this.isStarted = false;
    this.t=5
    this.timer = this.t;
    this.currentUser = this.route.snapshot.data.resolverUser;
    
    this.signalrService.startConnection();
    this.signalrService.connectionEstablished.subscribe(() => { 
      console.log(42);
      this.signalrService._hubConnection.on("NewEntry", data => {
        if(data != null && data.heartRate != 0)
        {
          if(this.lineChartData[0].data.length > 100)
          {
            this.lineChartData[0].data.splice(0,1)
            this.lineChartLabels.splice(0,1)
          }
          this.lineChartData[0].data.push(data.heartRate);
          this.lineChartLabels.push(this.format(new Date(data.time)));
          data.idSession = this.idSession;
          this.tab.push(data);
          console.log(this.tab.length)
          if(this.tab.length >= this.t && this.isStarted)
          {
            this.stop();                     
          }
          if(this.tab.length > 100 && !this.isStarted)
            this.tab = [];
        } 
      })
    })
  }

  format(date: Date) : string{
    let h = date.getHours();
    let m = date.getMinutes();
    let s = date.getSeconds();
    return `${this.pad(h)}:${this.pad(m)}:${this.pad(s)}`;
  }

  pad(d) {
    return (d < 10) ? '0' + d.toString() : d.toString();
  }

  start(){
      this.isStarted = true;
      this.tab = [];
      this.startTimer();
      this.toastr.success("La session est démarée");
  }

  stop(){
    this.isStarted = false;
    this.pauseTimer()
    if(this.tab.length >= (this.t-1))
    {
      this.entryService.add(this.tab).subscribe()    
      this.toastr.success("La session est terminé et uploader")
      this.tab = [];  
    }
    else
    {
      this.sessionService.delete(this.idSession).subscribe((response) =>{
        console.log("deleted");
      });
      this.toastr.success("La session est trop courte pour être sauvegardée");
      this.tab = []; 
    }   
  }

  startTimer() {
    this.isStarted = true;
    this.createSession(this.currentUser.id);
    this.interval = setInterval(() => {
      this.timer--;
      if(this.timer<0)
      {
        this.pauseTimer;
        this.stop();
      }
    },1000)
  }

  pauseTimer() {
    clearInterval(this.interval);
    this.timer = this.t;
  }

  createSession(id: number) {
    this.sessionService.add({idUser:id,time:new Date()})
    .subscribe(data => {
      this.toastr.success('Nouvelle session crée');
      this.idSession = data;
    })  
  }

  changeUser(){
    this.router.navigateByUrl('');
  }
}
