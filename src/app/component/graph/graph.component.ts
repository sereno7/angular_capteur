import { Component, OnInit } from '@angular/core';
import { Entry } from '../../models/entry';
import { EntryService} from '../../services/entry.service'
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})
export class GraphComponent implements OnInit {
  model: Entry[];
  constructor(private entryService : EntryService) { }

  public lineChartData: ChartDataSets[] = [
    { data: [], label: 'Series A' },
  ];
  public lineChartLabels: Label[] = [];

  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    annotation: "",
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      yAxes: [{
        ticks: {
           max: 600,
           min: 400
        }
     }]
    }
  }



  ngOnInit(): void {
    this.entryService.context.subscribe(data =>{
      if(data != null)
      {
        this.lineChartData[0].data = [];
        this.lineChartLabels = [];
        this.model = data.reverse();
        this.model.forEach(elt => {
          this.lineChartData[0].data.push(elt.heartRate);
          this.lineChartLabels.push(new Date(elt.time).toTimeString())
        });
      }
    })
    setInterval(() => {
      this.entryService.refresh();
    },2000)
    
  }

}
