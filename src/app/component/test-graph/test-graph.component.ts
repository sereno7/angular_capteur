import { preserveWhitespacesDefault } from '@angular/compiler';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';

@Component({
  selector: 'app-test-graph',
  templateUrl: './test-graph.component.html',
  styleUrls: ['./test-graph.component.scss']
})
export class TestGraphComponent implements OnInit {
  public lineChartData: ChartDataSets[] = [
    {data: [], label: ""},
    {data: [], label: ""},
    {data: [], label: ""}
  ];
  
  public lineChartLabels: Label[] = ['0:00', '0:30', '1:00', '1:30', '2:00', '2:30', '3:00'];

  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    annotation: "",
    responsive: true,
    scales: {
      yAxes: [
        {
          position: 'left',
          
          ticks: {
            max: 100,
            min: 40
          }
        }
      ]
    },
  };
  // public opacity1;
  public lineChartColors: Color[] = [
    { // yellow
      // backgroundColor: 'rgba(240, 232, 10,0.2)',
      backgroundColor: 'rgba(240, 232, 10,0.2)',
      borderColor: 'rgba(240, 232, 10,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // red
      // backgroundColor: 'rgba(255,0,0,0.3)',
      backgroundColor: 'rgba(255,0,0,0.2)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // green
      // backgroundColor: 'rgba(11, 191, 59,0.2)',
      backgroundColor: 'rgba(11, 191, 59,0.2)',
      borderColor: 'rgba(11, 191, 59,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
  ];
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';

  public test: any[] = [
    {
      heartRate: [80, 70, 65, 71, 74, 80, 73],
      time: "08/10/2020"
    },
    {
      heartRate: [72, 63, 85, 75, 79, 74, 90],
      time: "19/10/2020"
    },
    {
      heartRate: [65, 85, 80, 81, 72, 79, 84],
      time: "20/10/2020"
    },
    {
      heartRate: [75, 70, 60, 87, 69, 80, 72],
      time: "14/12/2020"
    },
    {
      heartRate: [73, 72, 65, 74, 85, 90, 89],
      time: "14/08/2020"
    },
    {
      heartRate: [68, 65, 69, 74, 72, 73, 80],
      time: "05/06/2020"
    }
  ];

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  constructor() { }

  public selectedDay: string = '';
  
  public buttons = document.getElementsByTagName('button');
  public ranges = document.querySelectorAll("input[type=range]");
  public opacity = [0.2, 0.2, 0.2];

  changeOpacity (event: any) {
    let order = event.target.getAttribute("order");
    let opacity = this.opacity[order - 1];
    opacity = event.target.value;
    this.opacity[order - 1] = opacity;
    this.lineChartColors[0].backgroundColor = `rgba(240, 232, 10,${this.opacity[0]})`;
    this.lineChartColors[1].backgroundColor = `rgba(255, 0, 0,${this.opacity[1]})`;
    this.lineChartColors[2].backgroundColor = `rgba(11, 191, 59,${this.opacity[2]})`;
  }

  public selectedValues = [];
  selectChangeHandler (event: any) {

    const e = event.target;
    // for(let i = 0; i < e.options.length; i++){
    //   const text = e.options[i].innerText;
    //   if(text !== "Choose your record") {
    //     e.options[i].disabled = false;
    //   }
    // }
    this.selectedDay = e.value;

    let pos = this.test.map(function(e) { return e.time; }).indexOf(this.selectedDay);

    // e.options[e.selectedIndex].disabled = true;

    const order = e.getAttribute("order");
    switch (order) {

      case "1":
        this.lineChartData[0].data = [];
        for (let i = 0; i < this.test[pos].heartRate.length; i++) {
          this.lineChartData[0].data.push(this.test[pos].heartRate[i]);
        }
        this.lineChartData[0].label = this.test[pos].time;
        this.selectedValues[0] = this.test[pos].time;
        break;

      case "2":
        this.lineChartData[1].data = [];
        for (let i = 0; i < this.test[pos].heartRate.length; i++) {
          this.lineChartData[1].data.push(this.test[pos].heartRate[i]);
        }
        this.lineChartData[1].label = this.test[pos].time;
        this.selectedValues[1] = this.test[pos].time;
        break;

      case "3":
        this.lineChartData[2].data = [];
        for (let i = 0; i < this.test[pos].heartRate.length; i++) {
          this.lineChartData[2].data.push(this.test[pos].heartRate[i]);
        }
        this.lineChartData[2].label = this.test[pos].time;
        this.selectedValues[2] = this.test[pos].time;
        break;
    }

    const isHidden = this.chart.isDatasetHidden(order - 1);
    if(!isHidden) {
      this.buttons[order - 1].disabled = false;
      let ranges = (document.querySelectorAll("input[type=range]"));
      (ranges[order - 1] as HTMLInputElement).disabled = false;
      this.buttons[order - 1].innerText = "Masquer"
    }
  }

  clickedSelect() {
    for(let i = 0; i < this.selects.length; i++) {
      for(let j = 1; j < this.selects[i].options.length; j++) {
          if(this.selects[i].options[j].value !== this.selectedValues[i] && this.selects[i].options[j].value !== "Choose your record") {
            this.selects[i].options[j].disabled = false;
          }
      }
    }
    for(let i = 0; i < this.selects.length; i++) {
      for(let j = 1; j < this.selects[i].options.length; j++) {
        if(this.selectedValues[i] !== null) {
          for(let k = 0; k < this.selectedValues.length; k++) {
            if(this.selects[i].options[j].value == this.selectedValues[k]) {
              this.selects[i].options[j].disabled = true;
            }
          }
        }
      }
    }

  }

  public selects = document.getElementsByTagName("select");

  ngOnInit() {
  }


  public hideOne(event: any){
    let pos: number = this.test.map(function(e) { return e.time; }).indexOf(this.selectedDay);
    const order = event.target.getAttribute("order");
    const isHidden = this.chart.isDatasetHidden(order - 1);
    this.chart.hideDataset(order - 1, !isHidden);
    if(!isHidden) {
      this.buttons[order - 1].innerText = "Afficher";
    }
    else {
      this.buttons[order - 1].innerText = "Masquer";
    }
  }
}