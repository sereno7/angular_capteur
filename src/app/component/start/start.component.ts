import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/users.service';
import { User } from '../../models/user';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SessionsService } from '../../services/sessions.service';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';



@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss']
})
export class StartComponent implements OnInit {
  model : User[]
  fg: FormGroup;
  historyChecked:boolean = false;
  
  constructor(
    private UserService : UserService,
    private SessionService : SessionsService,
    private toastr: NbToastrService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.UserService.context.subscribe(data =>{
      this.model = data; 
    })
    this.fg = new FormGroup({
      'Name': new FormControl(null, Validators.compose([
        Validators.required,
        Validators.maxLength(50)
      ])),
      'Password': new FormControl(null,Validators.compose([
        Validators.required,
        Validators.maxLength(50)
      ])),
    });
    this.UserService.refresh();
  }
  
  check(){
    let trouve = false;
    let id:number;
    for (const u of this.model) {
      if(u.name == this.fg.value.Name && u.password == this.fg.value.Password){
        trouve = true;
        id = u.id;
        this.toastr.success('Connecté');
        if(!this.historyChecked)
          this.router.navigateByUrl('/livegraph/' + u.id)
        else
          this.router.navigateByUrl('/realdata/' + u.id)
      }   
    }
    if(!trouve){
      this.UserService.add(this.fg.value).subscribe(data => {
        id = data;
        this.toastr.success('Compte crée');
        this.toastr.success('Connecté');
        this.router.navigateByUrl('/livegraph/' + id);
      });      
    }
  }

liked(isChecked: boolean){
  if(isChecked)
  {
    this.historyChecked = true;
    console.log(this.historyChecked);
  }  
  else
  {
    this.historyChecked = false;
    console.log(this.historyChecked);
  }
}
}
