export const environment = {
  production: true,
  ENTRY_BASE__URL : 'http://serenotb.azurewebsites.net/api/entry',
  USER_BASE__URL : 'http://serenotb.azurewebsites.net/api/user',
  USERDETAILS_BASE__URL : 'http://serenotb.azurewebsites.net/api/user/details',
  SESSIONS_BASE__URL : 'http://serenotb.azurewebsites.net/api/sessions/',
  SESSIONS_DETAIL_BYUSERID_BASE__URL : 'http://serenotb.azurewebsites.net/api/sessions/detailsbyuserid/',
  SIGNALR_URL : "http://serenotb.azurewebsites.net/entryhub/"
};
