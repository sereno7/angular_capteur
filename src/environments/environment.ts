// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // ENTRY_BASE__URL : 'http://localhost:57273/api/Entry/',
  // SIGNALR_URL : "http://localhost:57273/entryhub/"
  // ENTRY_BASE__URL : 'http://localhost/api_heartreate/api/entry',
  // USER_BASE__URL : 'http://localhost/api_heartreate/api/user',
  // USERDETAILS_BASE__URL : 'http://localhost/api_heartreate/api/user/details',
  // SESSIONS_BASE__URL : 'http://localhost/api_heartreate/api/sessions/',
  // SESSIONS_DETAIL_BYUSERID_BASE__URL : 'http://localhost/api_heartreate/api/sessions/detailsbyuserid/',
  // SIGNALR_URL : "http://localhost/api_heartreate/entryhub/"

  ENTRY_BASE__URL : 'http://serenotb.azurewebsites.net/api/entry',
  USER_BASE__URL : 'http://serenotb.azurewebsites.net/api/user',
  USERDETAILS_BASE__URL : 'http://serenotb.azurewebsites.net/api/user/details',
  SESSIONS_BASE__URL : 'http://serenotb.azurewebsites.net/api/sessions/',
  SESSIONS_DETAIL_BYUSERID_BASE__URL : 'http://serenotb.azurewebsites.net/api/sessions/detailsbyuserid/',
  SIGNALR_URL : "http://serenotb.azurewebsites.net/entryhub/"
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
